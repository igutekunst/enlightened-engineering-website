# Our Process

We work with clients who need electrical, software and hardware engineering services to deliver a new product, or enhance an existing one.

1. Initial Meeting
  During our initial meeting, we laern a bit more about you and your goals, and answer questions about us. We want both a technical and philosophical fit. 

2. Feasibility Study
  If we both perecieve there could be a good match, we will gather enough information about your requirements to perform a feasibility study. A feasiibility study ensures we examine key areas of difficulty as soon as possible, to avoid commiting to a project that we cannot deliver on. In some cases, this will be complimentary, and in other cases, this could be a long process with multiple paid milestones.

3. Cost Structure and Full Contract
  After determine a project is possible, we will work with you to define a cost structure that is mutually agreeable. We typically preffer hourly, or milestone based projects. In some cases we can arrange other terms.

4. Regular Check-ins
  As work begins, we will keep you informed of our progress with demos/writeups, or partially completed work. We want to be sure you see progress as it happens, and are available to work through any hurdles we might encounter.
