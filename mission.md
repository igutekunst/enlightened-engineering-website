# What is Enlightened Engineering

Enlightened Engineering is on a mission to enable technology to liberate humanity from scarcity.

Enlightened Engineering is on a mission to live in harmony with our environment. We believe humans as conscious being must learn to live in harmony with the universe while striving towards the good, the true and the beautiful.

As we have the ability to shape our environment with tools and technology, we also have the responsibility to do so consciously. 

Technology is the name we give to arrangements of matter that are the physical manifestation of human will. 

As human understanding of our environment has increased drastically since the western enlightenment, we’ve reached a point where scarcity of resources could be a thing of the past. We have not achieve this goal, becuase the zero sum mentallity is strongly engrained in our culture. 

Enlightened Engineering seeks to put an end to scarcity to recognizing that knowledge is infinite, and sharing knowledge, does not diminish the source of the idea. We are entering an era where some people are willing and able to see beyond individual competition as a zero sum game, and see that working as a global tribe is beneficial to all.

This isn’t simply the idealistic idea that we should help all people, but what is best for the individual is in fact what is best for all. 

Due to our ability to modify our environment in unprecentent ways, we’ve reach a point where our technology is sufficient to destroy us by damaging our environment beyond repair. Pollution in the air, water and land, and the mental sphere as well is making our environment toxic and less conducive to human thriving. This can no longer be dismissed as fear mongering, or via derogatory remarks about tree hugging environmentalists. This is a practical matter of survival. A person would not accept slowly piling trash in their living room, cutting away pieces of the roof for fuel to heat the home, nor accept noxious fumes from open fires, and yet we as a species do these same things on a larger scale, to our collective home that is this planet. We are already at a point where the earth cannot support these behaviors without tangible consequences to life, human and otherwise. 

Those of us who are fortunate enough and willing to look rationally have the opportunity to reimagine our relationship with ourselves and our home.  Millions of people throughout the world are imitating change through companies, social movements, non-profit organizations, books and more. The green movement has been upon us for a while.

As an engineer, I see the world partially through the lense of shaping matter and energy towards human goals. As an electrical and computer engineer, I ask myself how my knowledge can best be put to use. 


The patent system was created to allow entrepreneurs the opportunity to develop and commercialize ideas without the fear that established large companies will use their resources to stifle competition.

The reality doesn’t quiet match the vision anymore. Large companies are often in possession of extensive patent portfolios, guarded with elite law professionals that more often than not stifle innovation rather than protect the new entrepreneur. It is time to challenge the patent system as it exists.

The Open Source or Libre movement recognizes that sharing information is beneficial to all. 

The success of Linux operating system as the backbone of the modern web, and a pillar of many aspects of modern computing shows that giving away an idea rather than simply commercializing it and protecting it raises the playing field so all players are closer to the stars... 

The mobile computing revolution has drastically lowered the price for many components, including processors, sensors, batteries, cameras, displays and more. These technologies have made many other industries possible: drones, electric scooters, electric cars, gopro, new cameras, more.

By making the knowledge needed to produce advanced technology public, Enlightened Engineering lowers the barrier of to making meaningful change. 

There is vast amounts of knowledge locked within corporate walls. Much of this knowledge is discovered and reinvented in every new company. A lack of cooperation slows progress.



## Topic Sentences
Arduino and maker movement

Patent system was created with the idea that ideas should become public at some point to benefit all.

Open source is taking this idea to the extreme.

Resilient communities possessing the manufacturing base to support some or all of their needs without devolving into subsistence

Sustainable technology built to last, rather than to be discarded every year (like phones) 

Arbitraging of human right... multi national corporations using cheapest possible labor by building products in places with the least protection of human rights. 

Technology built to serve human thriving rather than profit for a few corporate entities

Remove the gatekeeper to innovation

Right to Repair and digital rights management. 

Grow the pie / raise the playing field instead of tilting it

All humans are equally valuable. Conscious is sacred



## Risks/ Arguments against 

Large companies have the resources to out compete small companies. If designs are open, they can easily be copied. This makes it risky for a company developing open hardware to invest too much money. (Interestingly, this might be a good thing, because it will keep open hardware companies small? But what about big problems?) 

Open electronics can’t save the world. Why aren’t you solving global warming? Or curing COVID? This is our strength. Play to your strengths. 

What’s wrong with existing solar products? (Right to repair, longevity, ability to modify and customize)

What’s the point? How can this possibly save the world? 

Aren’t people always going to be greedy? 

Hasn’t technology created lots of these problems? Wouldn’t it be better to live at once with nature? 

## Success Stories
Despite the risks of releasing designs in the open, there are two high profile success stories, and many smaller companies making money in open hardware. 

- Sparkfun Electronics
- Adafruit 


## Key areas of focus

Small scale advanced manufacturing 
	- Electronics 
		- Industrial control
		- Green building and automation
		-  Communication infrastructure
		- Environmental monitoring  
		- Art and entertainment 
	- Small plastic and metal parts
	- Motors and motion
	- Manufacturing Knowledge
		- Small manufacturing book
		- Design for manufacturing
		- Automated testing
		- Reliability and longevity
		- Safety 
	- 
