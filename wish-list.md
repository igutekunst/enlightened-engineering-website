# Wish List

These are technologies that we believe are fundamental enough that no one should worry about re-implementing them, nor pay exorbitant prices for.

We aim to design open source, UL listed products for each of these items.


## Power Supplies and Converters

 - AC-DC adapters (wall warts)
 - Battery Chargers
 - Solar
  - MPPT
  - Inverters
  - DC-DC
- Wind
- Geo-Thermal

## Test Equipment

 - Multimeters
 - Power analizers (fancy multimeters)
 - Signal generators
 - Oscilloscopes
 - Logic Analizers
 - Trace Probes
 - RF shenanigans 

## Communication Equipment

 - Network switches
 - Network Routers
 - Wireless access points
 - Wireless protocols
 - Radios in general
 - Communication software (P2P, IPFS, all the hipster things)

If you would like to fund development of one of these, please reach out.
