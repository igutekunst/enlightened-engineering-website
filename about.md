# About Us

Enlightened Engineering is an organization dedicated to building the fundamental pieces requried to build a good and _enlightened_ world.

We are a loosly associated group of engineers who come together to enable customers who believe in our to realize their visions. We work with people who understand the value of open source, both individually, and wholistically. (See [Philosophy](./philosophy.md)). 


We work with customers who want all or part of the design files and knowledge to be made available to the public with an Open Source licence. 

Customers retain copyright of some or all of the work output while a liberal licences allows others to benefit from and contribute back. 


For existing projects, we can use both open source and proprietary inputs. However, some of our delivers MUST be released as open source. This could mean that customer code remains confidential, while some supporting tools developed during the project are made public, or that some parts of the code that do not reval a competitive advantage are released.

