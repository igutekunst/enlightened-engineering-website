# Enlightened Engineering Website Repository

Content for website, and general brainstorming for advertising/marketing.


## Sections
 - [About](./about.md)
 - [Process](./process.md)
 - [Mission](./mission.md)
 - [Wish List](./wish-list.md)
 - [Project Ideas](./project-ideas.md)
