# Project Ideas

This is a brainstorm section of project ideas that would be good first projects to the company off the ground.


## Universal Power Adapter

### Description

The univeral power adapter allows you to power almost any laptop, phone, tablet, and many other DV powered devices from almost any power source. It is useful as a day to day charger, travel charger, camping charger, or even disaster charger.

### Specs

  One input: accepts AC or DC, 5V-400V (240V RMS)
  Output: USB C, maybe USB A, maybe some generic plug
  100 watts

### Use Cases
 - Every day charging of gadgets
 - Camping
 - Emergency
 - Mobile work


## MPPT/Charger

### Description
A small (100-500watt) solar power system, that will charge a lead acid or lithium battery and provide DC output for various devices.

### Specs
  - Solar Input
  - USB C, USB A output, maybe 12V output, maybe AC output
  - Battery connection for 12V lead acid, lithium, or maybe included battery


### Similar products:
 - https://www.rei.com/product/170008/goal-zero-yeti-lithium-200x-portable-power-station


## Power Inverter (1-phase to 3-phase) and Power Converter

### Description

A bi-directional power inverter that can take in DC and produce 120-240AC (1 phase, neutral hot ground) and AC and produce AC. Such an inverter is key towards solar, wind, and other renewable
deployments. It would be awesome if this could also produce 3 phase power for high power machinery (workspaces, labs, etc)

## Battery Fast Charger

### Description

Battery fast charger compatible with EV standards (possibly eVTOL as well). This is kind of an interesting one.

## Motor Controller / Inverter / Generator

### Description

Motion is a key element of many tasks, taking power and turning it into mechanical power to do work, or visa-versa. Could be useful for wind deployments, in addition to the solar deployments discussed in the MPPT section.


## PCB Assembly and Testing Line

### Description
A PCB assembly line where all machines and processes are open source.

 - Solder paste applicator 
 - Pick and place
 - Reflow Oven
 - Testing (bed of nails)


## Industrial Servo
### Description
High performance servo using BLDC + something like the ODrive. Could maybe be implemented on an FPGA with a RISC-V.

Possibly partner with existing motor manufacturer and add the motor controller/encoder.

## Small Farming/Gardening Robot

### Description 

A multi axis robotic arm with a vision system and multiple end affectoirs to perform some or all of the labor needed to maintain a large garden or small farm.

The robot should be able to plant seeds, water plants, pull weeds, thin seedlings, find pests, spray (organic :) pest repellant), and harvest produce.

Removing plants at the end of the growing season, and tilling the earth would be awesome.

### Similar and Related Projects

- [FarmBot](https://farm.bot)


